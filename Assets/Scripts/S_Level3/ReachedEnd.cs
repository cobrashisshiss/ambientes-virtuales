﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachedEnd : MonoBehaviour
{
    [SerializeField] GameObject door;
    [SerializeField] GameObject player;

    public LevelChanger lc;

    void Update()
    {
            float dist = Vector3.Distance(door.transform.position, player.transform.position);
            if (dist < 2f)
            {
                lc.FadeToLevel(4);
                Destroy(door);
                Destroy(this);
            }
    }
}
