﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPad : MonoBehaviour
{
    public int code;
    float disableTimer = 0;
    public AudioSource teleported;


    void Update()
    {

        //code = Random.Range(0, 4);
        if (disableTimer > 0)
            disableTimer -= Time.deltaTime;
    }

    void OnTriggerEnter (Collider collider)
    {
        if (collider.gameObject.name == "RigidBodyFPSController" && disableTimer <=0)
        {
            teleported.Play();
            foreach (TeleportPad tp in FindObjectsOfType<TeleportPad>())
            {
                if (tp.code == code && tp != this)
                {
                    tp.disableTimer = 2;
                    Vector3 position = tp.gameObject.transform.position;
                    position.y += 1;
                    collider.gameObject.transform.position = position;
                }
            }
        }

    }
}
