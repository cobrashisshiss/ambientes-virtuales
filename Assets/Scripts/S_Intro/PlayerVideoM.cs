﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class PlayerVideoM : MonoBehaviour
{
    [SerializeField] RawImage rawImage;
    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] LevelChanger lc;

    // Start is called before the first frame update
    void Start()
    {


        videoPlayer.loopPointReached += LoadScene;
    }

    private void Update()
    {
        
    }

    void LoadScene(VideoPlayer vp)
    {
        lc.FadeToLevel(1);
    }
}