﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SkipButton : MonoBehaviour
{
    public LevelChanger lc;
    public void SkipVideo()
    {
        //SceneManager.LoadScene("Level1");
        lc.FadeToLevel(1);
    }
}
