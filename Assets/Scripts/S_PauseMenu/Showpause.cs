﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Showpause : MonoBehaviour
{
    [SerializeField] GameObject canvasPrefab;
    

    //cuando iniciamos el nivel seteamos el canvasprefab en falso 
    void Start()
    {
        canvasPrefab.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            canvasPrefab.SetActive(true);
        }
    }
}
