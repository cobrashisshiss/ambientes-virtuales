﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseButton : MonoBehaviour {

    //[SerializeField] LevelChanger lc;
    private int sceneIndex;
    private LevelChanger levelCh;
    private UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController player;
    [SerializeField] GameObject canvasPrefab;

   
    private void Awake()
    {
        //llamamos al firstperson para poder accesar al bool de menuActive
        GameObject ply = GameObject.Find("RigidBodyFPSController");
        player = ply.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>();
        player.resumeBtPressed = false;

    }
    private void Start()
    {
        //Encotramos el objeto y luego buscamos suu componente para  accesar a el 
        GameObject lvlC = GameObject.Find("LevelChanger");
        levelCh = lvlC.GetComponent<LevelChanger>();
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        player.resumeBtPressed = false;

    }
    private void Update()
    {
        
    }

    public void ResumeLevel()
    {
        //player.menuActive = false;
        player.resumeBtPressed = true;
        canvasPrefab.SetActive(false);
    }

    public void RestartLevel()
    {
        
        levelCh.FadeToLevel(sceneIndex);
    }
    public void MainMenu()
    {
        levelCh.FadeToLevel(0);
    }
   
}
