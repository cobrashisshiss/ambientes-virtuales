﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    private int keyCounterBad = 0;
    private int keyCounterGood = 0;
    private bool soundPlay = false;
    [SerializeField]
                GameObject door,
                audioDoorObj;
    public GameObject Player;
    [SerializeField] AudioSource audioDoor;


    public LevelChanger lc;

    [HideInInspector] public bool Finished = false;

    private void Start()
    {
        
    }
    void Update()
    {
        //float dist = Vector3.Distance(door.transform.position, Player.transform.position);

        if (keyCounterBad == 3 && keyCounterGood == 3)
        {
            if (soundPlay == false)
            {
                StartCoroutine(PlaySoundOnce());
                soundPlay = true;
            }

           
            float distPlayer = Vector3.Distance(Player.transform.position, audioDoorObj.transform.position);
            if (distPlayer < 2)
            {
                DoorHasBeenUnlocked();
            }
            
        }

    }

    public void GoodKeyAdd(int keyAdd)
    {
        keyCounterGood += keyAdd;
        print(keyCounterGood + "goodCest");
    }

    public void BadKeyAdd(int keyAdd)
    {
        keyCounterBad += keyAdd;
        print(keyCounterBad + "BadCest");
    }

    private void DoorHasBeenUnlocked()
    {

        lc.FadeToLevel(3);
       
        Destroy(this);
    }
    IEnumerator PlaySoundOnce()
    {
        yield return new WaitForSeconds(1);
        audioDoor.Play();
        yield break;
        //StopCoroutine(PlaySoundOnce());
    }
}
