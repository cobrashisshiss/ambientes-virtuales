﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PickUpObj : MonoBehaviour
{
    [SerializeField] Transform dest;
    private bool emptyHand = true;
    [SerializeField] GameObject player;
    [SerializeField] Vector3 dist;
    public AudioSource droped;
    public AudioSource picked;


    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {

    }
    public void PickUp()
        {
        picked.Play();
        emptyHand = false;
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
        NavMeshAgent nm = this.gameObject.GetComponent<NavMeshAgent>();
        Destroy(rb);
        Destroy(nm);

        this.transform.position = dest.position;
        this.transform.parent = GameObject.Find("Hand").transform;
    }

    public void TrowAway()
    {
        
        droped.Play();
        
        emptyHand = true;
        Rigidbody rb = this.gameObject.AddComponent<Rigidbody>();


        this.transform.parent = null;
    }
    
}
