﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Raycast : MonoBehaviour
{
    [SerializeField] float rayLenght;
    public LayerMask layerMask;
    PickUpObj obj;
    private bool emptyHand = true;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            LaunchRay();
        }
        if (Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject() && emptyHand == false)
        {
            Release();
        }

    }

    private void LaunchRay()
    {
       
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, rayLenght, layerMask)&& emptyHand)
            {

            obj = hit.transform.gameObject.GetComponent<PickUpObj>();
            obj.PickUp();

         
            emptyHand = false;
            }
       
    }
    private void Release()
    {
        obj.TrowAway();
        emptyHand = true;
    }
}

