﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    GameObject obj;
    Vector3 dist;
    Manager manager;
    bool hasEspejo = false;
    bool hasBotella = false;
    bool hasJoya = false;
    bool hasCuchillo = false;
    bool hasPhone = false;
    bool hasGun;
    [SerializeField] GameObject goodCest;
    [SerializeField]
    GameObject espejoObj,
        botellaObj,
        joyaObj,
        cuchilloObj,
        phoneObj,
        gunObj;

    private void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<Manager>();
    }

    private void Update()
    {
        StartCoroutine(ChkDistEspejo());
        StartCoroutine(ChkDistBotella());
        StartCoroutine(ChkDistJoya());
        StartCoroutine(ChkDistCuchillo());
        StartCoroutine(ChkDistPhone());
        StartCoroutine(ChkDistGun());

    }

    private IEnumerator ChkDistEspejo()
    {
        yield return new WaitForSeconds(0.5f);
        float distEspejo = Vector3.Distance(goodCest.transform.position, espejoObj.transform.position);
        if (distEspejo < 1f && hasEspejo == false)
        {
            manager.GoodKeyAdd(1);
            hasEspejo = true;
            print("you added Espejo");
        }
        if (distEspejo > 2 && hasEspejo == true)
        {
            manager.GoodKeyAdd(-1);
            hasEspejo = false;
            print("you rested Espejo");
        }

    }
    private IEnumerator ChkDistBotella()
    {
        yield return new WaitForSeconds(0.5f);
        float distBotella = Vector3.Distance(goodCest.transform.position, botellaObj.transform.position);
        if (distBotella < 1 && hasBotella == false)
        {
            manager.GoodKeyAdd(1);
            hasBotella = true;
            print("you added Botella");
        }
        if (distBotella > 2 && hasBotella == true)
        {
            manager.GoodKeyAdd(-1);
            hasBotella = false;
            print("you rested Botella");
        }

    }
    private IEnumerator ChkDistJoya()
    {
        yield return new WaitForSeconds(0.5f);
        float distBotella = Vector3.Distance(goodCest.transform.position, joyaObj.transform.position);
        if (distBotella < 1f && hasJoya == false)
        {
            manager.GoodKeyAdd(1);
            hasJoya = true;
            print("you added Joya");
        }
        if (distBotella > 2 && hasJoya == true)
        {
            manager.GoodKeyAdd(-1);
            hasJoya = false;
            print("you rested joya");
        }


    }
    private IEnumerator ChkDistCuchillo()
    {
        yield return new WaitForSeconds(0.5f);
        float distBotella = Vector3.Distance(goodCest.transform.position, cuchilloObj.transform.position);
        if (distBotella < 1 && hasCuchillo == false)
        {
            manager.GoodKeyAdd(1);
            hasCuchillo = true;
            print("you added Cuchillo");
        }
        if (distBotella > 2 && hasCuchillo == true)
        {
            manager.GoodKeyAdd(-1);
            hasCuchillo = false;
            print("you rested Cuchillo");
        }

    }
    private IEnumerator ChkDistPhone()
    {
        yield return new WaitForSeconds(0.5f);
        float distEspejo = Vector3.Distance(goodCest.transform.position, phoneObj.transform.position);
        if (distEspejo < 1 && hasPhone == false)
        {
            manager.GoodKeyAdd(1);
            hasPhone = true;
            print("you added Phone");
        }
        if (distEspejo > 2 && hasPhone == true)
        {
            manager.GoodKeyAdd(-1);
            hasPhone = false;
            print("you rested Phone");
        }

    }
    private IEnumerator ChkDistGun()
    {
        yield return new WaitForSeconds(0.5f);
        float distEspejo = Vector3.Distance(goodCest.transform.position, gunObj.transform.position);
        if (distEspejo < 1 && hasGun == false)
        {
            manager.GoodKeyAdd(1);
            hasGun = true;
            print("you added Gun from G");
        }
        if (distEspejo > 2 && hasGun == true)
        {
            manager.GoodKeyAdd(-1);
            hasGun = false;
            print("you rested Gun from G");
        }

    }
}
