﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AmbientManager : MonoBehaviour
{

    public Sound[] Sounds;
    public static AmbientManager instance;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in Sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {
        Play("RoomsAmbient");
    }

    public void Play (string name)
    {

        Sound s = Array.Find(Sounds, sound => sound.name == name);
        if (s == null)
            return;
        s.source.Play();
    }
}
