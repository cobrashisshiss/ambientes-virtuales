﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshSound : MonoBehaviour
{

    public GameObject AudioObject;

    void Start()
    {
        AudioObject = GameObject.Find("AmbientManager");

    }

    void Update()
    {
        Destroy(AudioObject);
    }
}
