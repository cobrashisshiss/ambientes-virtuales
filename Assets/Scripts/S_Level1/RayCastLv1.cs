﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RayCastLv1 : MonoBehaviour
{
    //Script para mouse

    [SerializeField] GameObject counterObj,
                     ratObj;
    private Counter countScript;
    private GotTheRat ratScript;

    [SerializeField] float rayLenght;
    public LayerMask layerMask;
    PickUpObj obj;
    private bool emptyHand = true;



    private void Start()
    {
        //Guardamos el counter para poder cambiarlo despues
        countScript = counterObj.GetComponent<Counter>();
        ratScript = ratObj.GetComponent<GotTheRat>();
        

    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            LaunchRay();
        }
        if (Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject())
        {
            Release();
        }

    }
    private void LaunchRay()
    {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, rayLenght, layerMask) && emptyHand)
        {
            if (hit.transform.gameObject.name == "Llave")
            {
                obj = hit.transform.gameObject.GetComponent<PickUpObj>();
                obj.PickUp();
                countScript.KeyPicked(true);
                emptyHand = false;
            }
            if (hit.transform.gameObject.name == "Llave2")
            {
                obj = hit.transform.gameObject.GetComponent<PickUpObj>();
                obj.PickUp();
                countScript.Key2Picked(true);
                emptyHand = false;
                
            }

            if (hit.transform.gameObject.name == "PickRAT")
            {
                obj = hit.transform.gameObject.GetComponent<PickUpObj>();
                obj.PickUp();
                ratScript.RatPicked(true);
                emptyHand = false;

            }

        }

    }

    private void Release()
    {
        obj.TrowAway();
        emptyHand = true;
    }
}
