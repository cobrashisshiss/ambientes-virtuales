﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Counter : MonoBehaviour
{
    [HideInInspector] public bool hasKey = false;
    [HideInInspector] public bool hasKey2 = false;
    [SerializeField] GameObject door;
    [SerializeField] GameObject player;

    public LevelChanger lc;
    public AudioSource opened;

    private void Start()
    {
        
    }

   

    void Update()
    {
        
        //Checamos si tenemos la llave
        if (hasKey)
        {
            float dist = Vector3.Distance(door.transform.position, player.transform.position);
            //print("You Have the key");
            //print(dist);
            if (dist < 2f)
            {
                lc.FadeToLevel(2);
                //Destroy(door);
                Destroy(this);
            }


        }
        if (hasKey2)
        {
            float dist = Vector3.Distance(door.transform.position, player.transform.position);
            if (dist < 2f)
            {
                lc.FadeToLevel(6);
                //Destroy(door);
                Destroy(this);
            }


        }
    }

    //esta se manda a llamar desde el PickUpKey 
    public void KeyPicked(bool picked)
    {
        opened.Play();

        hasKey = picked;
    }
    public void Key2Picked(bool picked)
    {
        opened.Play();

        hasKey2 = picked;
        
    }

   
}
