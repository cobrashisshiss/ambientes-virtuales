﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject objectToSpawn;
    public int numOfEnemies;
    private float spawnRad = 1;
    private Vector3 spawnPosition;
    int enemiesSpawned = 0;

    void Start()
    {
    }

     void Update()
    {
        if (enemiesSpawned < 5)
        {
            SpawnObject();
        }
    }

    void SpawnObject()
    {
        for (int i = 0; i < numOfEnemies; i++)
        {
            spawnPosition = transform.position + Random.insideUnitSphere * spawnRad;
            Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
        }

          enemiesSpawned++;
    }
}
