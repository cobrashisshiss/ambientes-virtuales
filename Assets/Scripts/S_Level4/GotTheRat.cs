﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotTheRat : MonoBehaviour
{
    [SerializeField] GameObject door;
    [SerializeField] GameObject player;

    [HideInInspector] public bool hasRat = false;


    public LevelChanger lc;

    void Update()
    {
        float dist = Vector3.Distance(door.transform.position, player.transform.position);

        if (dist < 2f && hasRat)
        {
            lc.FadeToLevel(5);
            //Destroy(door);
            Destroy(this);
        }
    }

    public void RatPicked(bool picked)
    {

        hasRat = picked;
    }
}
