﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetGame : MonoBehaviour
{
   
    public LevelChanger lc;


     void Start()
    {
        StartCoroutine("Wait");

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(33);

        lc.FadeToLevel(0);
    }
}
