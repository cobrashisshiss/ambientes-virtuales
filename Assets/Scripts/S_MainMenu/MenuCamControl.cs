﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MenuCamControl : MonoBehaviour
{
    
    [SerializeField] Transform startPos;
    public Transform currentmount;
    //public Button firstButton;
    //public Camera cam;
    public float speedfactor;
    //public float zoomfactor;
    public Vector3 lastposition;
    
    // Use this for initialization
    private void Awake()
    {
        this.transform.position = new Vector3(startPos.position.x, startPos.position.y, startPos.position.z);
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, currentmount.position, speedfactor);
        transform.rotation = Quaternion.Slerp(transform.rotation, currentmount.rotation, speedfactor);


    }

    public void Setmount(Transform newmount)
    {
        currentmount = newmount;
    }

    void StartHighlight()
    {
        
    }
}
