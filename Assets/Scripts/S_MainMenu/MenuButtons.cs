﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    [SerializeField]
    GameObject
        mainMenu,
        settingsMenu;


    public LevelChanger lc;

    private void Awake()
    {
        settingsMenu.SetActive(false);
    }

    public void StartButton()
    {
        lc.FadeToLevel(7);
    }
    public void C_settings()
    {
        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }

    public void S_MainMenu()
    {
        mainMenu.SetActive(true);
        settingsMenu.SetActive(false);
    }


    public void ModeSelect()
    {
        StartCoroutine("Wait");

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(33);

        lc.FadeToLevel(0);
    }

    public void C_exit()
    {
        lc.FadeToLevel(0);

    }

    public void C_quitGame()
    {
        lc.FadeToLevel(0);
        Application.Quit();
    }
}
