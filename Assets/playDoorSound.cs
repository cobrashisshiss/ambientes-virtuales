﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playDoorSound : MonoBehaviour
{

    public AudioSource opened;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "RigidBodyFPSController")
        {
            opened.Play();
        }
    }
}
