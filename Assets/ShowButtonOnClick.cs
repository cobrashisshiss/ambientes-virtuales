﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowButtonOnClick : MonoBehaviour
{
    [SerializeField] GameObject bt;

    // Start is called before the first frame update
    private void Awake()
    {
        bt.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)|| Input.GetKeyDown(KeyCode.Space))
        {
            bt.SetActive(true);
        }
    
    }
    
}
